exports.ConferenceWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var ConferenceDetailWindow = require('ui/ConferenceDetailWindow');
	
	globals.tblConference = Ti.UI.createTableView({
		top: 0
	});
		
	globals.tblConference.addEventListener('click', function(e) {
		var row = e.row;
		var cID = row.cID;
		var cTitle = row.cName;		
		var rowIndex = e.index;
		var conferenceDetailWindow = new ConferenceDetailWindow({ title: cTitle, cID: cID, backgroundColor: globals.appBGColor, barColor: globals.navBarColor });
		globals.tabs.currentTab.open(conferenceDetailWindow,{animated:true});
	});
	
	instance.add(globals.tblConference);
	instance.add(globals.ai);
	
	instance.addEventListener('focus', function(e) {
		globals.data.getConferences();
	});
	
	return instance;
};