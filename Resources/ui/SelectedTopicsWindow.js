exports.SelectedTopicsWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var SelectedTopicDetailWindow = require('ui/SelectedTopicDetailWindow');
		
	var searchBar = Ti.UI.createSearchBar({
		barColor: globals.toolbarColor,
		showCancel: false,
		hintText: 'Search Selected Topics'
	});
	
	searchBar.addEventListener('focus', function(e) {
		searchBar.showCancel = false;
	});
	
	globals.stTable = Ti.UI.createTableView({
		filterAttribute: 'title',
		search: searchBar,
		top: 0
	});
	
	globals.stTable.addEventListener('click', function(e) {
		var row = e.row;
		var stID = row.stID;
		var topicTitle = row.title;
		var rowIndex = e.index;
		var selectedTopicWindow = new SelectedTopicDetailWindow({ title: topicTitle, backgroundColor: globals.appBGColor, barColor: globals.navBarColor });
		globals.data.getSelectedTopic(stID,selectedTopicWindow);
	});
	instance.add(globals.stTable);
	instance.add(globals.ai);
	
	instance.addEventListener('focus', function(e) {
		var selectedTopicsData = globals.data.getSelectedTopics();
	});
	
	return instance;
};