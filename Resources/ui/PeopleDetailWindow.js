exports.PeopleDetailWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var pID = args.pID;
	var pData;
	for (var x in globals.dataPeople) {
		if (globals.dataPeople[x].NUMBER == pID) {
			pData = globals.dataPeople[x];
		}
	}
	
	var data = [], row, lblLeft, lblRight;
	
	var lblName = Ti.UI.createLabel({
		height: 50,
		text: pData.NAME,
		top: 12,
		left: 12,
		font: { fontSize: 20, fontWeight: 'bold' },
		shadowColor: '#ffffff',
	    shadowOffset:{ x:1, y:1}
	});
	
	var tableView = Ti.UI.createTableView({
		style: Ti.UI.iPhone.TableViewStyle.GROUPED,
		backgroundColor: 'transparent',
		rowBackgroundColor: 'white',
		top: 50
	});
	
	/*
	 * Contact 1 label
	 */
	if (pData.CONTACT.length > 0) {
		row = Ti.UI.createTableViewRow({height: 35});
		
		lblLeft = Ti.UI.createLabel({
			text: pData.CONTACT,
			top: 0,
			left: 8,
			font: { fontSize: 17, fontWeight: 'bold' }
		});
		row.add(lblLeft);
		
		if (pData.POSITION.length > 0) {
			var lblPosition = Ti.UI.createLabel({
				text: pData.POSITION,
				top: 35,
				left: 8,
				color: '#666',
				font: { fontSize: 13 }
			});
			row.height = 70;
			row.add(lblPosition);
		}
 		
 		data.push(row);
	}
	
	/*
	 * Email 1 label
	 */
	if (pData.EMAIL.length > 0){
		lblLeft = Ti.UI.createLabel({
			text: 'email',
			top: 0,
			left: 8,
			width: 80,
			textAlign: 'right',
			color: '#1078d8',
			font: { fontSize: 14, fontWeight: 'bold' }
		});
		
		lblRight = Ti.UI.createLabel({
			text: pData.EMAIL,
			top: 0,
			left: 100,
			font: { fontSize: 16, fontWeight: 'bold' }
		});
		
		lblRight.addEventListener('click', function() {
			globals.utils.sendEmail(pData.EMAIL);
		});
		
		row = Ti.UI.createTableViewRow({height: 35});
		row.add(lblLeft,lblRight);
		data.push(row);
	}
	
	/*
	 * Phone 1 label 
	 */
	if (pData.TELEPHONE.length > 0){
		lblLeft = Ti.UI.createLabel({
			text: 'phone',
			top: 0,
			left: 8,
			width: 80,
			textAlign: 'right',
			color: '#1078d8',
			font: { fontSize: 14, fontWeight: 'bold' }
		});
		
		lblRight = Ti.UI.createLabel({
			text: pData.TELEPHONE,
			top: 0,
			left: 100,
			font: { fontSize: 16, fontWeight: 'bold' }
		});
		
		lblRight.addEventListener('click', function() {
			/*
			 * Remove characters from phone number
			 */
			ph = pData.TELEPHONE.replace(/\D/g,'');
			Ti.Platform.openURL('tel:'+ph);
		});
		
		row = Ti.UI.createTableViewRow({height: 35});
		row.add(lblLeft,lblRight);
		data.push(row);
	}
	
	/*
	 * TTY label 
	 */
	if (pData.TTY.length > 0){
		lblLeft = Ti.UI.createLabel({
			text: 'phone tty',
			top: 0,
			left: 8,
			width: 80,
			textAlign: 'right',
			color: '#1078d8',
			font: { fontSize: 14, fontWeight: 'bold' }
		});
		
		lblRight = Ti.UI.createLabel({
			text: pData.TTY,
			top: 0,
			left: 100,
			font: { fontSize: 16, fontWeight: 'bold' }
		});
		
		row = Ti.UI.createTableViewRow({height: 40});
		row.add(lblLeft,lblRight);
		data.push(row);
	}
	
	/*
	 * FAX label 
	 */
	if (pData.FAX.length > 0){
		lblLeft = Ti.UI.createLabel({
			text: 'fax',
			top: 0,
			left: 8,
			width: 80,
			textAlign: 'right',
			color: '#1078d8',
			font: { fontSize: 14, fontWeight: 'bold' }
		});
		
		lblRight = Ti.UI.createLabel({
			text: pData.FAX,
			top: 0,
			left: 100,
			font: { fontSize: 16, fontWeight: 'bold' }
		});
		
		row = Ti.UI.createTableViewRow({height: 40});
		row.add(lblLeft,lblRight);
		data.push(row);
	}
	
	/*
	 * Address label
	 */
	var addressString = '', addressHeight = 20;
	if (pData.ADDRESS.length > 0) {
		if (pData.ADDRESS.length < 10) {
			for (var x in pData.ADDRESS) {
				addressString += pData.ADDRESS[x]+'\r\n';
				addressHeight = parseInt(addressHeight+20);
			}	
		} else {
			addressString += pData.ADDRESS+'\r\n';
			addressHeight = parseInt(addressHeight+20);
		}	
		
		addressString += pData.CITY+' '+pData.STATE+', '+pData.ZIP;
		addressHeight = parseInt(addressHeight+20);
		
		var lblAddress = Ti.UI.createLabel({
			text: addressString,
			top: 0,
			left: 8,
			font: { fontSize: 16 }
		});
		row = Ti.UI.createTableViewRow({height: addressHeight});
 		row.add(lblAddress);
 		data.push(row);
	}
	
	tableView.setData(data);
	instance.add(lblName);	
	instance.add(globals.ai);
	instance.add(tableView);
	
	return instance;
}	