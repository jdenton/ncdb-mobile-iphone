exports.PeopleWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var PeopleDetailWindow = require('ui/PeopleDetailWindow');
	
	var searchBar = Ti.UI.createSearchBar({
	   barColor: globals.toolbarColor,
	   showCancel: false,
	   hintText: 'Search people & programs'
	});
	
	searchBar.addEventListener('focus', function(e) {
		searchBar.showCancel = false;
	});
 
	//AUTOCOMPLETE TABLE
	globals.tblAutocomplete = Ti.UI.createTableView({
	   search: searchBar,
	   filterAttribute: 'title',
	   top: 0
	});
	instance.add(globals.ai);
	instance.add(globals.tblAutocomplete);
	
	/*
	 * Search bar events
	 */
	var lastSearch = null, timer;
	searchBar.addEventListener('change', function(e) {
		if (searchBar.value.length > 2 && searchBar.value !=  lastSearch) {			
			clearTimeout(timer);
		    timer = setTimeout(function() {
		    	lastSearch = searchBar.value;
		    	globals.data.peopleSearch(searchBar.value);
		    }, 300);
		  	
   		}
   		return false;
	});
	
	globals.tblAutocomplete.addEventListener('click', function(e) {
		var row = e.row;
		var pID = row.pID;
		var pTitle = row.title;
		var rowIndex = e.index;
		var peopleDetailWindow = new PeopleDetailWindow({ title: pTitle, pID: pID, backgroundColor: globals.appBGColor, barColor: globals.navBarColor });
		globals.tabs.currentTab.open(peopleDetailWindow,{animated:true});
	});
	
	return instance;
};