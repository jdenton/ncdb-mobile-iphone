exports.ConferenceDetailWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var cID = args.cID;
	var ui = {};
	var mObj = {};
	
	var scrollView = Ti.UI.createScrollView({ 
		contentWidth:'auto',
		top: 0, 
		showVerticalScrollIndicator: false, 
		showHorizontalScrollIndicator: false 
	});
	
	var tableView = Ti.UI.createTableView({
		style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
		backgroundColor: 'transparent',
		rowBackgroundColor:'white'
	});
	
	ui.lblTitle = Ti.UI.createLabel({
		height: 50,
		top: 4,
		left: 12,
		font: { fontSize: 20, fontWeight: 'bold' },
		shadowColor: '#ffffff',
	    shadowOffset:{ x:1, y:1},
	});
	
	ui.viewAddress = Ti.UI.createView({
		width: 'auto',
		height: 50,
		top: 60,
		left: 0
	});
	
	ui.lblAddress = Ti.UI.createLabel({
		width: 250,
		height: 50,
		left: 12,
		color: '#666',
		font: { fontSize: 13 }
	});
	
	ui.btnMap = Ti.UI.createButton({
		title: 'Map',
		color: '#000000',
		backgroundImage: 'none',
		borderRadius: 4,
		borderColor: '#9acc01',
		borderWidth: 1,
		font: {
			fontSize: 12,
			fontWeight: 'bold'			
		},
		selectedColor: 'none',
		selectedImage: 'none',
	    backgroundGradient: globals.styles.gradient('green'),
		height: 30,
		width: 50,
		left: 250
	});
	
	ui.viewAddress.add(ui.lblAddress,ui.btnMap);
	
	scrollView.add(ui.lblTitle);
	scrollView.add(ui.viewAddress);
	scrollView.add(tableView);
	instance.add(globals.ai);
	instance.add(scrollView);
	
	instance.addEventListener('focus', function(e) {
		globals.data.getConference(cID);
	});
	
	globals.populateConferenceUI = function(jsonData) {
		var data = jsonData[0];
		Ti.API.info(data);
		ui.lblTitle.text = data.NAME;
		var rowData = [], row;
		
		/*
		 * Create map, if we're given an address
		 */
		var addressString = '', addressRender = '';
		if (data.ADDRESS.length < 10) {
			for (var x in data.ADDRESS) {
				addressString += data.ADDRESS[x]+' ';
				addressRender += data.ADDRESS[x]+'\r\n';
			}
		} else {
			addressString += data.ADDRESS+' ';
			addressRender += data.ADDRESS+'\r\n';
		}	
		
		addressString += data.CITY+' '+data.STATE+' '+data.ZIP;
		addressRender += data.CITY+' '+data.STATE+' '+data.ZIP;
		if (addressString.length > 20) {			
			ui.lblAddress.text = addressRender;
			ui.viewAddress.add(ui.lblAddress,ui.btnMap);
			
			mObj.Address = addressString;
			mObj.PlaceName = data.NAME;
			
			ui.btnMap.addEventListener('click', function(e) {
				MapModalWindow.init(mObj);
				MapModalWindow.win.open({
					modal: true,
					modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
				});	
			});	
			
			tableView.setTop(115);
		} else {
			ui.viewAddress.visible = false;
			tableView.setTop(65);
		}
		
		/*
		 * Date label
		 */
		if (data.DATE.length > 0) {
			lblLeft = Ti.UI.createLabel({
				text: 'date',
				top: 0,
				left: 8,
				width: 80,
				textAlign: 'right',
				color: '#1078d8',
				font: { fontSize: 14, fontWeight: 'bold' }
			});
		
			lblRight = Ti.UI.createLabel({
				text: data.DATE,
				top: 0,
				left: 100,
				font: { fontSize: 16, fontWeight: 'bold' }
			});
	 		row = Ti.UI.createTableViewRow({height: 35});
			row.add(lblLeft,lblRight);
			rowData.push(row);
		}
		
		/*
		 * Contact 1 label
		 */
		if (data.CONTACT.length > 0) {
			lblLeft = Ti.UI.createLabel({
				text: 'contact',
				top: 0,
				left: 8,
				width: 80,
				textAlign: 'right',
				color: '#1078d8',
				font: { fontSize: 14, fontWeight: 'bold' }
			});
		
			lblRight = Ti.UI.createLabel({
				text: data.CONTACT,
				top: 0,
				left: 100,
				font: { fontSize: 16, fontWeight: 'bold' }
			});
	 		row = Ti.UI.createTableViewRow({height: 35});
			row.add(lblLeft,lblRight);
			rowData.push(row);
		}
		
		/*
		 * Phone 1 label
		 */
		if (data.TELEPHONE.length > 0) {
			lblLeft = Ti.UI.createLabel({
				text: 'phone',
				top: 0,
				left: 8,
				width: 80,
				textAlign: 'right',
				color: '#1078d8',
				font: { fontSize: 14, fontWeight: 'bold' }
			});
		
			lblRight = Ti.UI.createLabel({
				text: data.TELEPHONE,
				top: 0,
				left: 100,
				font: { fontSize: 16, fontWeight: 'bold' }
			});
	 		row = Ti.UI.createTableViewRow({height: 35});
			row.add(lblLeft,lblRight);
			rowData.push(row);
		}
		
		/*
		 * Email 1 label
		 */
		if (data.CONTACT.length > 0) {
			lblLeft = Ti.UI.createLabel({
				text: 'email',
				top: 0,
				left: 8,
				width: 80,
				textAlign: 'right',
				color: '#1078d8',
				font: { fontSize: 14, fontWeight: 'bold' }
			});
		
			lblRight = Ti.UI.createLabel({
				text: data.EMAIL,
				top: 0,
				left: 100,
				font: { fontSize: 16, fontWeight: 'bold' }
			});
			lblRight.addEventListener('click', function() {
				globals.utils.sendEmail(data.EMAIL);
			});
	 		row = Ti.UI.createTableViewRow({height: 35});
			row.add(lblLeft,lblRight);
			rowData.push(row);
		}
		
		/*
		 * Website URL label
		 */
		if (data.URL.length > 0) {
			lblLeft = Ti.UI.createLabel({
				text: 'url',
				top: 0,
				left: 8,
				width: 80,
				textAlign: 'right',
				color: '#1078d8',
				font: { fontSize: 14, fontWeight: 'bold' }
			});
		
			lblRight = Ti.UI.createLabel({
				text: data.URL,
				top: 0,
				left: 100,
				font: { fontSize: 16, fontWeight: 'bold' }
			});
			lblRight.addEventListener('click', function() {
				Ti.Platform.openURL(data.URL);
			});
	 		row = Ti.UI.createTableViewRow({height: 35});
			row.add(lblLeft,lblRight);
			rowData.push(row);
		}
		
		/*
		 * Summary
		 */
		if (data.SUMMARY.length > 0) {
			var lblSummary = Ti.UI.createLabel({
				text: data.SUMMARY,
				top: 4,
				left: 8,
				width: 290,
				font: { fontSize: 14 }
			});
			
			var rowHeight = globals.utils.calcContainerHeightForString(data.SUMMARY);
			row = Ti.UI.createTableViewRow({height: rowHeight});
			
			row.add(lblSummary);
	 		rowData.push(row);
	 	}	
		
		tableView.setData(rowData);
	}
	
	return instance;
};	