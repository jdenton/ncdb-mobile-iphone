exports.AboutWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	
	var viewLogo = Ti.UI.createImageView({
		top: 8,
		url:'images/logo.gif',
		height: 70, 
		width: 300,
		borderColor: '#a9a76c',
		backgroundColor: '#c1bf8e',
		borderRadius: 5,
		borderWidth: 1
	});
		
	var lblAbout = Ti.UI.createLabel({
		text: 'This app is brought to you by the NCDB. Contact us by any of the following:',
		top: 85,
		left: 12,
		height: 50,
		font: { fontSize: 15, fontWeight: 'bold' },
		shadowColor: '#ffffff',
	    shadowOffset:{ x:1, y:1}
	});
	
	var tblAbout = Ti.UI.createTableView({
		top: 120,
		style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
		backgroundColor: 'transparent',
		rowBackgroundColor:'white'
	});
	
	var rowData = [];
	var lblLeft = Ti.UI.createLabel({
		text: 'url',
		top: 0,
		left: 8,
		width: 60,
		textAlign: 'right',
		color: '#1078d8',
		font: { fontSize: 14, fontWeight: 'bold' }
	});
		
	var lblRight = Ti.UI.createLabel({
		text: globals.SITE_URL,
		top: 0,
		left: 80,
		font: { fontSize: 16, fontWeight: 'bold' }
	});
	
	lblRight.addEventListener('click', function() {
		Ti.Platform.openURL(globals.SITE_URL);
	});
	var row = Ti.UI.createTableViewRow({height: 35});
	row.add(lblLeft,lblRight);
	rowData.push(row);
	
	lblLeft = Ti.UI.createLabel({
		text: 'phone',
		top: 0,
		left: 8,
		width: 60,
		textAlign: 'right',
		color: '#1078d8',
		font: { fontSize: 14, fontWeight: 'bold' }
	});
		
	lblRight = Ti.UI.createLabel({
		text: '800-438-9376',
		top: 0,
		left: 80,
		font: { fontSize: 16, fontWeight: 'bold' }
	});
	
	lblRight.addEventListener('click', function() {
		Ti.Platform.openURL('tel:800-438-9376');
	});
	row = Ti.UI.createTableViewRow({height: 35});
	row.add(lblLeft,lblRight);
	rowData.push(row);
	
	lblLeft = Ti.UI.createLabel({
		text: 'email',
		top: 0,
		left: 8,
		width: 60,
		textAlign: 'right',
		color: '#1078d8',
		font: { fontSize: 14, fontWeight: 'bold' }
	});
		
	lblRight = Ti.UI.createLabel({
		text: globals.emailMain,
		top: 0,
		left: 80,
		font: { fontSize: 16, fontWeight: 'bold' }
	});
	
	lblRight.addEventListener('click', function() {
		globals.utils.sendEmail(globals.emailMain);
	});
	row = Ti.UI.createTableViewRow({height: 35});
	row.add(lblLeft,lblRight);
	rowData.push(row);
	
	tblAbout.setData(rowData);
	instance.add(viewLogo);
	instance.add(lblAbout);
	instance.add(tblAbout);
	return instance;
};