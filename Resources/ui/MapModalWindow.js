/*
 * MapModalWindow.js
 * =========================================
 * This file provides the modal window for viewing 
 * a map for conferences.
 */

MapModalWindow = {};
MapModalWindow.win = Ti.UI.createWindow({ 
	barColor: '#593d5a',
	backgroundColor: '#fff'
});

MapModalWindow.init = function(mObj) {
	var address = mObj.Address;
	var placeName = mObj.PlaceName;
	
	MapModalWindow.win.title = address;
		
	var btnMapWindowClose = Ti.UI.createButton({
		title: 'Close',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	btnMapWindowClose.addEventListener('click', function() {
		MapModalWindow.win.close();
	});
	MapModalWindow.win.setRightNavButton(btnMapWindowClose);
	
	var mapView = Ti.Map.createView({
	    mapType: Ti.Map.STANDARD_TYPE,
	    animate: true,
	    regionFit: true,
	    userLocation: true
	});
	MapModalWindow.win.add(mapView);
	
	/*
	 * Ti.Geolocation.forwardGeocoder doesn't work anymore 
	 * so do the following...
	 */
	var xhrCoords = Ti.Network.createHTTPClient();
	xhrCoords.open('GET', 'http://maps.googleapis.com/maps/geo?output=json&q=' + address);
	xhrCoords.onload = function() {
	    var json = JSON.parse(this.responseText);
	    var lat = json.Placemark[0].Point.coordinates[1];
	    var log = json.Placemark[0].Point.coordinates[0];
	    Ti.API.info(lat+' '+log);
	    mapView.region = {latitude: lat, longitude: log, latitudeDelta:0.01, longitudeDelta:0.01}
	    
	    var mapAnnotation = Ti.Map.createAnnotation({
		    latitude: lat,
		    longitude: log,
		    title: placeName,
		    subtitle: address,
		    pincolor: Ti.Map.ANNOTATION_RED,
		    animate: true,
		    myid: 1
		});
		mapView.annotations = [mapAnnotation];
	};
	xhrCoords.send();
}	