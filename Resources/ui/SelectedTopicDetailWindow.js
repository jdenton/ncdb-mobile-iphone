exports.SelectedTopicDetailWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
		
	globals.stWebView = Ti.UI.createWebView({
		backgroundColor: '#ffffff',
		scalesPageToFit: false,
	    contentWidth:'auto',
	    contentHeight:'auto'
	});
	instance.add(globals.ai);
	instance.add(globals.stWebView);
	
	return instance;
}	