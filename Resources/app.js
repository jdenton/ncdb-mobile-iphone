Titanium.UI.setBackgroundColor('#000');
require('lib/requirePatch').monkeypatch(this);
var globals  = {};

/*
 * Create a private scope to prevent further polluting the global object
 */
(function() {
	Ti.include('ui/MapModalWindow.js');
	var AppTabGroup          = require('ui/AppTabGroup');
	var SelectedTopicsWindow = require('ui/SelectedTopicsWindow');
	var PeopleWindow         = require('ui/PeopleWindow');
	var ConferenceWindow     = require('ui/ConferenceWindow');
	var AboutWindow          = require('ui/AboutWindow');
	
	globals.styles = require('lib/styles');
	globals.utils  = require('lib/utilities');
	globals.data   = require('lib/remoteData');
	globals.dataPeople;
	globals.dataConference;
	
	globals.navBarColor       = '#593d5a';
	globals.appBGColor        = '#eeefe1';
	globals.toolbarColor      = '#c1bf8e';
	globals.emailMain         = 'dblink@tr.wou.edu';
	
	globals.ai = globals.utils.activityIndicator(); 	
	globals.SITE_URL = 'http://www.nationaldb.org/';
	
	globals.tabs = new AppTabGroup(
		{
			title: 'Topics',
			icon: 'images/icon_list_bullets.png',
			window: new SelectedTopicsWindow({title: 'Selected Topics', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
		},
		{
			title: 'People',
			icon: 'images/icon_users.png',
			window: new PeopleWindow({ title: 'People & Programs', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
		},
		{
			title: 'Conferences',
			icon: 'images/icon_calendar.png',
			window: new ConferenceWindow({ title: 'Conferences', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
		},
		{
			title: 'About',
			icon: 'images/icon_information.png',
			window: new AboutWindow({ title: 'About', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
		}
	);
	globals.tabs.open();
})();
