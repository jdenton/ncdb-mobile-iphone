exports.getSelectedTopics = function() {
	globals.ai._show({message: 'Loading...'});
	var stAJAX = Titanium.Network.createHTTPClient();
	stAJAX.onload = function() {
		globals.ai._hide();
		var jdata = JSON.parse(this.responseText);
		var rowData = [];
		
		stTable = globals.stTable;
		var header = '';
		for(var a=0;a<=jdata.length-1;a++) {
			var title = jdata[a].CatTitle;
			var newHeader = title.charAt(0).toUpperCase();
			
			if (title != '') {
				if (newHeader != header) {
					header = newHeader;
					rowData[a] = Ti.UI.createTableViewRow({ 
						hasDetail: true, 
						title: title,
						header: newHeader,
						stID: jdata[a].CatID
					});
				} else {
					rowData[a] = Ti.UI.createTableViewRow({ 
						hasDetail: true, 
						title: title,
						stID: jdata[a].CatID
					});	
				}		
			}		
		}	
		stTable.data = rowData;
	}	
	stAJAX.open('POST',globals.SITE_URL+'mobileFunctions.php');
	stAJAX.send({
		'getSelectedTopics'  : '1'
	});	 
}

exports.getSelectedTopic = function(topicCatID,window) {
	globals.ai._show({message: 'Loading...'});
	var stAJAX = Titanium.Network.createHTTPClient();
	stAJAX.onload = function() {
		globals.ai._hide();
		Ti.API.info(this.responseText);
		globals.stWebView.html = this.responseText;
		globals.tabs.currentTab.open(window,{animated:true});
	}	
	stAJAX.open('POST',globals.SITE_URL+'mobileFunctions.php');
	stAJAX.send({
		'getSelectedTopicContent'  : '1',
		'topicCatID' : topicCatID
	});	 
}

exports.peopleSearch = function(searchTerm) {
	globals.ai._show({message: 'Loading...'});
	var pAJAX = Titanium.Network.createHTTPClient();
	pAJAX.onload = function() {
		globals.ai._hide();
		var tblAutocomplete = globals.tblAutocomplete;
		var rowData = [];
		var jdata = JSON.parse(this.responseText);
		globals.dataPeople = jdata;
		
		var header = '', rowHeader, rowContent;
		for(var a=0;a<=jdata.length-1;a++) {
			var title = jdata[a].NAME;
			var newHeader = title.charAt(0).toUpperCase();
			
			if (title != '') {
				if (newHeader != header) {
					header = newHeader;
					rowData[a] = Ti.UI.createTableViewRow({ 
						hasDetail: true, 
						title: title,
						header: newHeader,
						pID: jdata[a].NUMBER
					});
				} else {
					rowData[a] = Ti.UI.createTableViewRow({ 
						hasDetail: true, 
						title: title,
						pID: jdata[a].NUMBER
					});	
				}		
			}	
			
		}
		tblAutocomplete.setData(rowData);
	}	
	pAJAX.open('POST',globals.SITE_URL+'mobileFunctions.php');
	pAJAX.send({
		'peopleSearch'  : '1',
		'searchTerm' : searchTerm
	});	 
}

exports.getConferences = function() {
	globals.ai._show({message: 'Loading...'});
	var cAJAX = Titanium.Network.createHTTPClient();
	cAJAX.onload = function() {
		globals.ai._hide();
		var tblConference = globals.tblConference;
		var rowData = [];
		Ti.API.info(this.responseText);
		var jdata = JSON.parse(this.responseText);
		globals.dataPeople = jdata;
		
		var header = '', rowHeader, rowContent,lblTitle, lblDate;
		for(var a=0;a<=jdata.length-1;a++) {
			var title = jdata[a].NAME;
			rowData[a] = Ti.UI.createTableViewRow({ 
				hasDetail: true, 
				height: 50,
				cID: jdata[a].NUMBER,
				cName: jdata[a].NAME
			});
			lblTitle = Ti.UI.createLabel({
				text: title,
				top: 7,
				height: 20,
				left: 8,
				font: { fontSize: 17, fontWeight: 'bold' }
			});
			lblDate = Ti.UI.createLabel({
				text: jdata[a].DATE,
				top: 22,
				left: 8,
				color: '#666',
				font: { fontSize: 13 }
			});
			rowData[a].add(lblTitle,lblDate);	
		}
		tblConference.setData(rowData);
	}	
	cAJAX.open('POST',globals.SITE_URL+'mobileFunctions.php');
	cAJAX.send({
		'getConferences'  : '1'
	});	 
}

exports.getConference = function(cID) {
	globals.ai._show({message: 'Loading...'});
	var cAJAX = Titanium.Network.createHTTPClient();
	cAJAX.onload = function() {
		globals.ai._hide();
		var jdata = JSON.parse(this.responseText);
		globals.populateConferenceUI(jdata);
	}	
	cAJAX.open('POST',globals.SITE_URL+'mobileFunctions.php');
	cAJAX.send({
		'getConference'  : '1',
		'cID' : cID
	});	 
}
