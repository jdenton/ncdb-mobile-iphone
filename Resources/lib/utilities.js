exports.trim = function(string) {
	return string.replace(/^\s+|\s+$/g,"");
}

exports.txtToField = function(string) {
    /*
     * Replace <br>, <br />
     */
    var newString  = string.replace(/<br>/g,"\r\n");
    newString = newString.replace(/<br\/>/g,"\r\n");

    /*
     * Replace ' and " html codes with actual characters
     */
    newString = newString.replace(/&#39;/g,"'");
    newString = newString.replace(/&#34;/g,'"');
    newString = newString.replace(/&quot;/g,'"');
    newString = newString.replace(/&amp;/g,'&');
    newString = newString.replace(/&nbsp;/g,' ');
        
    /*
     * Remove any other HTML tags like links
     */
    newString = globals.utils.stripTags(newString);
    return newString;
}

exports.stripTags = function(string) {
	return string.replace(/<([^>]+)>/g,'')
}

exports.removeAllPickerRows = function(picker) {
 
    var _col = picker.columns[0];
        var len = _col.rowCount;
        for(var x = len-1; x >= 0; x-- ){
            var _row = _col.rows[x]
            _col.removeRow(_row);
        }
        picker.reloadColumn(_col); 
}
 
exports.addPickerRows = function(_col,data) { 
    for(var x = 0; x < data.length; x++ ) { 
        _col.addRow(data[x]); 
    } 
}

exports.setAppBadge = function() {
	/*
	 * Check badgeTriggers for anything
	 */
	var messages = globals.badgeTriggers.messages;
	var tasks    = globals.badgeTriggers.tasks;
	var timers   = globals.badgeTriggers.timers;
	
	var badgeText = null;
	if (timers > 0) {
		badgeText = 1;
	} else if (messages > 0) {
		badgeText = messages;
	} else if (tasks > 0) {
		badgeText = tasks;
	}
	Ti.UI.iPhone.appBadge = badgeText;
	
	Ti.App.Properties.setString('timers', timers);
	Ti.App.Properties.setString('messages', messages);
	Ti.App.Properties.setString('tasks', tasks);
}

exports.escapeString = function(string) {
	var string2 = string.replace('"','\"');
	var string3 = string2.replace("'","\'");
	return string3;
	
}

function isAndroid() {
	return Ti.Platform.osname == 'android';
}

exports.activityIndicator = function() {
	var container;

	if (isAndroid()) {
		container = Ti.UI.createActivityIndicator({ color:'#fff' });	
	} else {
		container = Ti.UI.createView({ width: '100%', height: '100%', top: 0, left: 0, visible: false });

		container.add( Ti.UI.createView({ width: '100%', height: '100%', top: 0, left: 0, backgroundColor: '#000', opacity: 0.5, zIndex: 0 }) );

		var ai = Ti.UI.createActivityIndicator({ style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG, color:'#fff', zIndex: 100 });
		container.add( ai );
		ai.show();
		ai = null;
	}

	container._isShowing = false;

	container._show = function(params) {
		if (this._isShowing) {
			return;	
		}

		if (isAndroid()) {
			this.message = params.message;
		} else {
			this.children[1].message = params.message;
		}	

		if (params.timeout) {
			this._myTimeout = setTimeout(function() {
				exports.manager.get('ai')._hide();

				if (params.timeoutMessage) {
					var alertDialog = Ti.UI.createAlertDialog({
						title: 'Update Timeout',
						message: params.timeoutMessage,
						buttonNames: ['OK']
					});
					alertDialog.show();
				}
			}, params.timeout);	
		}

		this._isShowing = true;
		this.show();
	};

	container._hide = function() {
		if (this._myTimeout !== undefined) {
			clearTimeout(this._myTimeout);
			delete this._myTimeout;
		}
		if (this._isShowing) {
			this._isShowing = false;
			this.hide();
		}
	}

	return container;
};

exports.findIndexByKeyValue = function(obj, key, value) {
    for (var i = 0; i < obj.length; i++) {
    	if (typeof(obj[i][key].length) != 'undefined') {
    		globals.utils.findIndexByKeyValue(obj[i][key],key,value);
    	} else {
	        if (obj[i][key] == value) {
	            return i;
	        }
	    }    
    }
    return null;
}

exports.sortByProjectAsc = function(a,b) {
        if (a.ProjectTitle != null) {
            var x = a.ProjectTitle.toLowerCase();
        }
        if (b.ProjectTitle != null) {
            var y = b.ProjectTitle.toLowerCase();
        }
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

exports.number_format = function(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

exports.sendEmail = function(emailAddress,subject) {
	var emailDialog = Titanium.UI.createEmailDialog();
	if (typeof subject != 'undefined' && subject.length > 0) {
		emailDialog.subject = subject;
	}	
	emailDialog.toRecipients = [emailAddress];	
	emailDialog.open();
}

exports.calcContainerHeightForString = function(string,fontSize) {
	var totalChars = string.length;
	var lines = parseInt(totalChars/40);
	var totalHeight = parseInt(lines * 20);
	return totalHeight;
}
